README
######

This is a `Cookiecutter <https://cookiecutter.io>`_ template for a basic Python package. It is configured to use some of the best modern Python tooling, such as

- `setuptools <https://setuptools.pypa.io/en/latest/index.html>`_ as the build system
- `pytest <https://docs.pytest.org/en/latest/contents.html>`_ for automated testing
- `coverage.py <https://coverage.readthedocs.io/en/7.5.3/>`_ for measuring code coverage
- `pre-commit <https://github.com/pre-commit/pre-commit>`_ for pre-commit hooks
- `ruff <https://docs.astral.sh/ruff/>`_ for code formatting, linting, and other static analysis
- `mypy <https://www.mypy-lang.org>`_ for static typing

It comes with a Makefile that has some scripts for virtual environment management with `uv <https://github.com/astral-sh/uv>`_
