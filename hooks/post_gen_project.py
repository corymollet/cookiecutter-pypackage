
if '{{ cookiecutter.use_autoenv_for_automatic_virtualenv_activation }}' == "yes":
    with open(".autoenv.zsh", "w", encoding="utf-8") as autoenv:
        autoenv.writelines(
            [
                '[ -z "$VIRTUAL_ENV" ] && source ~/.envs/{{ cookiecutter.package_name }}/bin/activate\n',
            ]
        )


    with open(".autoenv_leave.zsh", "w", encoding="utf-8") as leave:
        leave.write("deactivate")
